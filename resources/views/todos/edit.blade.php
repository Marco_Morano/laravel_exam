@extends('layouts.template')


@section('content')
    <h1 class="mt-4">{{  Auth::user()->name }}, modifica il tuo todo !</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ route('todos.update',$todo->id) }}">
        @csrf
        @method('PUT')
        <label>Titolo Todo</label>
        <input type="text" name="titolo" class="form-control" value="{{$todo->titolo}}"/>
        <label>Testo Todo</label>
        <textarea name="testo" class="form-control"  rows="10" id="mytextarea">{{$todo->testo}}</textarea>
        <div class="row">
            <div class="col-6">
                <label>Priorità</label>
                <select class="form-control" name="priorita">
                    <option value="0" @if($todo->priorita == '0') selected @endif>Bassa</option>
                    <option value="1" @if($todo->priorita == '1') selected @endif>Medio</option>
                    <option value="2" @if($todo->priorita == '2') selected @endif>Alta</option>
                </select>
            </div>
            <div class="col-6">
                <label>Stato</label>
                <select class="form-control" name="stato">
                    <option value="1" @if($todo->stato == '1') selected @endif>Attivo</option>
                    <option value="0" @if($todo->stato == '0') selected @endif>Completato</option>
                </select>
            </div>
        </div>
        <br>

        <br />
        <input type="submit" value="Modifica Todo" class="btn btn-primary" />
    </form>
@endsection


