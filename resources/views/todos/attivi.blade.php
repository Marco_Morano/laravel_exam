@extends('layouts.template')


@section('content')
    <h1 class="mt-4">{{  Auth::user()->name }}, visualizza i tuoi todo attivi !</h1>




    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titolo</th>
            <th scope="col">Testo</th>
            <th scope="col">Azioni</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($todos as $todo)
            @php
                if ($todo->priorita == '0'){
                    $colonna = 'table-primary';
                }elseif ($todo->priorita == '1'){
                    $colonna = 'table-warning';
                }else{
                    $colonna = 'table-danger';
                }
            @endphp

            <tr class="@if($todo->stato == 1){{$colonna}}@else table-success @endif">

                <th scope="row">{{ $todo->id }}</th>
                <td>{{ $todo->titolo }}</td>
                <td>{!! $todo->testo  !!}</td>
                <td>
                    <div class="row">
                        <div class="col-3"><a href="/todos/{{ $todo->id }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a></div>
                        <div class="col-3">
                            <form method="post" action="{{ route('todos.destroy',$todo->id) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" value="Elimina" class="btn btn-primary"><i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </div>
                        <div class="col-3">
                            @if($todo->stato == "1")
                                <a href="/todos/{{ $todo->id }}/completa" class="btn btn-success"><i class="fas fa-check"></i></a>
                            @endif
                        </div>
                    </div>



                </td>

            </tr>

        @endforeach
        </tbody>
    </table>

@endsection
