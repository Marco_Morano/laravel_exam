@extends('layouts.template')


@section('content')
    <h1 class="mt-4">{{  Auth::user()->name }}, inserisci una nuova cosa da fare !</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ route('todos.store') }}">
        @csrf
        <label>Titolo Todo</label>
        <input type="text" name="titolo" class="form-control" />



        <label>Testo Todo</label>
        <textarea name="testo" class="form-control"  rows="10" id="mytextarea"></textarea>
        <label>Priorità</label>
        <br>
        <select class="form-control" name="priorita">
            <option value="0">Bassa</option>
            <option value="1">Medio</option>
            <option value="2">Alta</option>
        </select>
        <br />
        <input type="submit" value="Salva Todo" class="btn btn-primary" />
    </form>
@endsection


