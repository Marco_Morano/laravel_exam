<?php

use App\Models\Todo;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/todos/attivi',[TodoController::class, 'todo_attivi'])->name('todos.attivi');
Route::get('/todos/completati',[TodoController::class, 'todo_completati'])->name('todos.completati');
Route::resource('todos', TodoController::class)->middleware(['auth']);

Route::get('/todos/{id}/completa',[TodoController::class, 'completa'])->name('todos.completa');


