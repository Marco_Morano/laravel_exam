<?php

namespace Database\Seeders;

use App\Models\Todo;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Todo::insert([
            'titolo' => 'Comprare una mela',
            'testo' => "<p>Comprare una mela rossa</p>",
            'priorita' => 1,
            'stato' => 1,
            'created_at' => Carbon::now()->subDays(3)

        ]);
        Todo::insert([
            'titolo' => 'Comprare una rosa',
            'testo' => "<p>Comprare una rosa rossa</p>",
            'priorita' => 0,
            'stato' => 1,
            'created_at' => Carbon::now()->subDays(2)

        ]);
        Todo::insert([
            'titolo' => 'Comprare macchina',
            'testo' => "<p>Comprare una macchina verde</p>",
            'priorita' => 2,
            'stato' => 0,
            'created_at' => Carbon::now()->subDays(1),
            'fine' => Carbon::now()
        ]);
        Todo::insert([
            'titolo' => 'Comprare un computer nuovo',
            'testo' => "<p>Comprare una computer per programmare</p>",
            'priorita' => 2,
            'stato' => 1,
            'created_at' => Carbon::now()->subDays(5)
        ]);
    }
}
