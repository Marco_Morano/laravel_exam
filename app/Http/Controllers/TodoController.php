<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmationEmail;
use App\Models\Todo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::all();

        return view('todos.index',compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo;
        $todo->titolo = $request->titolo;
        $todo->testo = $request->testo;
        $todo->priorita = $request->priorita;
        $todo->stato = "1"; //1 corrisponde a attivo 0 a completato
        $todo->save();
        return redirect('/todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);

        return view('todos.edit',compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        $todo->titolo = $request->titolo;
        $todo->testo = $request->testo;
        $todo->priorita = $request->priorita;
        $todo->stato = $request->stato;
        if ($todo->stato == '0'){
            $todo->fine = Carbon::now();
        }
        $todo->save();
        return redirect('/todos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        $todo->delete();
        return redirect('/todos');
    }

    public function completa($id)
    {

        $todo = Todo::find($id);
        $todo->stato = '0';
        Mail::to(Auth::user()->email)->send(new ConfirmationEmail($todo, Auth::user()));
        $todo->save();
        return redirect('/todos');
    }

    public function todo_attivi()
    {
        $todos = Todo::where('stato', 1)->get();

        return view('todos.attivi',compact('todos'));
    }

    public function todo_completati()
    {
        $todos = Todo::where('stato', 0)->get();

        return view('todos.completati',compact('todos'));
    }
}
